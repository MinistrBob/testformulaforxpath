﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using FastColoredTextBoxNS;

namespace TestFormulaForXPath
{
    public partial class Form1 : Form
    {
        XmlDocument xmlfile = new XmlDocument();
        XmlNode xmlfile_root;
        //XmlDocument targetconfig = new XmlDocument();
        //XmlNode targetconfig_root;
        XmlNode xmlfilenode;
        string xmlstring;
        //XmlNode targetnode;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fastColoredTextBoxXML.Text = File.ReadAllText(openFileDialog1.FileName);
                    xmlfile.Load(openFileDialog1.FileName);
                    xmlfile_root = xmlfile.DocumentElement;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (xmlfile_root == null)
            {
                MessageBox.Show("Нужно открыть xml-файл");
                return;
            }
            // Ищем строку в параметре в source (старый конфиг)
            // Если нет алиаса - ищем так
            if (string.IsNullOrEmpty(textBoxAlias.Text) && string.IsNullOrEmpty(textBoxNamespace.Text))
            {
                try
                {
                    xmlfilenode = xmlfile_root.SelectSingleNode(textBoxFormula.Text);
                    xmlstring = xmlfile_root.SelectSingleNode(textBoxFormula.Text).Value;
                }
                catch (Exception)
                {

                    return;
                }
            }
            // Если алиас есть -ищем так
            else
            {
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlfile.NameTable);
                nsmgr.AddNamespace(textBoxAlias.Text, textBoxNamespace.Text);
                try
                {
                    xmlfilenode = xmlfile_root.SelectSingleNode(textBoxFormula.Text, nsmgr);
                    xmlstring = xmlfile_root.SelectSingleNode(textBoxFormula.Text, nsmgr).Value;
                }
                catch (Exception)
                {

                    return;
                }
            }

            //if (!(xmlfilenode == null))
            if (!(string.IsNullOrEmpty(xmlstring)))
            {
                lbInnerText.Text = xmlfilenode.InnerText;
                lbOuterText.Text = xmlfilenode.OuterXml;
                textBoxResult.Text = xmlstring;
            }
            else
            {
                textBoxResult.Text = "результат не найден";
            }

            //// Если строка не была найдена (например, она просто не была сконфигурирована) - просто пишем в лог и идем дальше
            //if (xmlfilenode == null)
            //{
            //    MessageBox.Show(string.Format("Элемент {0} не найден.", textBoxFormula.Text);

            //}
            //else
            //{
            //    //Context.Status.WriteLine(string.Format("найдено значение {0}", xmlfilenode.InnerText));
            //    if (string.IsNullOrEmpty(configelement.NameSpaceAlias) && string.IsNullOrEmpty(configelement.NameSpace))
            //    {
            //        targetnode = targetconfig_root.SelectSingleNode(configelement.XPath);

            //    }
            //    else
            //    {
            //        XmlNamespaceManager nsmgr = new XmlNamespaceManager(targetconfig.NameTable);
            //        nsmgr.AddNamespace(configelement.NameSpaceAlias, configelement.NameSpace);
            //        targetnode = targetconfig_root.SelectSingleNode(configelement.XPath, nsmgr);
            //    }
            //    // Изменяем элемент
            //    targetnode.InnerText = xmlfilenode.InnerText;
            //}
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            fastColoredTextBoxXML.Text = System.Xml.Linq.XDocument.Parse(fastColoredTextBoxXML.Text).ToString();
        }
    }
}
