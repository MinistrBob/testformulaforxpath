﻿namespace TestFormulaForXPath
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonLoadXML = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxAlias = new System.Windows.Forms.TextBox();
            this.textBoxNamespace = new System.Windows.Forms.TextBox();
            this.textBoxFormula = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.buttonFomatXML = new System.Windows.Forms.Button();
            this.fastColoredTextBoxXML = new FastColoredTextBoxNS.FastColoredTextBox();
            this.lbInnerText = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbOuterText = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBoxXML)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonLoadXML
            // 
            this.buttonLoadXML.Location = new System.Drawing.Point(19, 18);
            this.buttonLoadXML.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonLoadXML.Name = "buttonLoadXML";
            this.buttonLoadXML.Size = new System.Drawing.Size(112, 35);
            this.buttonLoadXML.TabIndex = 0;
            this.buttonLoadXML.Text = "LoadXML";
            this.buttonLoadXML.UseVisualStyleBackColor = true;
            this.buttonLoadXML.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 91);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(543, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Алиас для пространства имен (namespace) (если есть), например \"bk\"";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 158);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(671, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Пространство имен (namespace) (если есть), например \"urn:nhibernate-configuration" +
    "-2.2\"";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 238);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(706, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Формула для запроса Xpath, например \"appSettings/add[@key=\'DocumentArchiveUrl\']/@" +
    "value\"";
            // 
            // textBoxAlias
            // 
            this.textBoxAlias.Location = new System.Drawing.Point(23, 123);
            this.textBoxAlias.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxAlias.Name = "textBoxAlias";
            this.textBoxAlias.Size = new System.Drawing.Size(772, 26);
            this.textBoxAlias.TabIndex = 5;
            // 
            // textBoxNamespace
            // 
            this.textBoxNamespace.Location = new System.Drawing.Point(23, 186);
            this.textBoxNamespace.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxNamespace.Name = "textBoxNamespace";
            this.textBoxNamespace.Size = new System.Drawing.Size(772, 26);
            this.textBoxNamespace.TabIndex = 6;
            // 
            // textBoxFormula
            // 
            this.textBoxFormula.Location = new System.Drawing.Point(23, 263);
            this.textBoxFormula.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxFormula.Name = "textBoxFormula";
            this.textBoxFormula.Size = new System.Drawing.Size(702, 26);
            this.textBoxFormula.TabIndex = 7;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(756, 257);
            this.buttonSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(112, 35);
            this.buttonSearch.TabIndex = 8;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.button2_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 309);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Результат";
            // 
            // textBoxResult
            // 
            this.textBoxResult.Location = new System.Drawing.Point(23, 334);
            this.textBoxResult.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.Size = new System.Drawing.Size(772, 26);
            this.textBoxResult.TabIndex = 10;
            // 
            // buttonFomatXML
            // 
            this.buttonFomatXML.Location = new System.Drawing.Point(178, 18);
            this.buttonFomatXML.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonFomatXML.Name = "buttonFomatXML";
            this.buttonFomatXML.Size = new System.Drawing.Size(112, 35);
            this.buttonFomatXML.TabIndex = 11;
            this.buttonFomatXML.Text = "FomatXML";
            this.buttonFomatXML.UseVisualStyleBackColor = true;
            this.buttonFomatXML.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // fastColoredTextBoxXML
            // 
            this.fastColoredTextBoxXML.AutoScrollMinSize = new System.Drawing.Size(35, 22);
            this.fastColoredTextBoxXML.BackBrush = null;
            this.fastColoredTextBoxXML.CharHeight = 22;
            this.fastColoredTextBoxXML.CharWidth = 12;
            this.fastColoredTextBoxXML.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fastColoredTextBoxXML.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.fastColoredTextBoxXML.IsReplaceMode = false;
            this.fastColoredTextBoxXML.Location = new System.Drawing.Point(19, 508);
            this.fastColoredTextBoxXML.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.fastColoredTextBoxXML.Name = "fastColoredTextBoxXML";
            this.fastColoredTextBoxXML.Paddings = new System.Windows.Forms.Padding(0);
            this.fastColoredTextBoxXML.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.fastColoredTextBoxXML.Size = new System.Drawing.Size(908, 303);
            this.fastColoredTextBoxXML.TabIndex = 12;
            this.fastColoredTextBoxXML.Zoom = 100;
            // 
            // lbInnerText
            // 
            this.lbInnerText.AutoSize = true;
            this.lbInnerText.Location = new System.Drawing.Point(23, 369);
            this.lbInnerText.Name = "lbInnerText";
            this.lbInnerText.Size = new System.Drawing.Size(76, 20);
            this.lbInnerText.TabIndex = 13;
            this.lbInnerText.Text = "InnerText";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(27, 393);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(768, 26);
            this.textBox1.TabIndex = 14;
            // 
            // lbOuterText
            // 
            this.lbOuterText.AutoSize = true;
            this.lbOuterText.Location = new System.Drawing.Point(27, 426);
            this.lbOuterText.Name = "lbOuterText";
            this.lbOuterText.Size = new System.Drawing.Size(79, 20);
            this.lbOuterText.TabIndex = 15;
            this.lbOuterText.Text = "OuterText";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(31, 451);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(764, 26);
            this.textBox2.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 829);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.lbOuterText);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lbInnerText);
            this.Controls.Add(this.fastColoredTextBoxXML);
            this.Controls.Add(this.buttonFomatXML);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.textBoxFormula);
            this.Controls.Add(this.textBoxNamespace);
            this.Controls.Add(this.textBoxAlias);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonLoadXML);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBoxXML)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLoadXML;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxAlias;
        private System.Windows.Forms.TextBox textBoxNamespace;
        private System.Windows.Forms.TextBox textBoxFormula;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Button buttonFomatXML;
        private FastColoredTextBoxNS.FastColoredTextBox fastColoredTextBoxXML;
        private System.Windows.Forms.Label lbInnerText;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lbOuterText;
        private System.Windows.Forms.TextBox textBox2;
    }
}

